function triTableau(colonne) {
    var tableau, lignes, switching, i, x, y, devraitEchanger;
    tableau = document.getElementById("tableau");
    switching = true;

    while (switching) {
        switching = false;
        lignes = tableau.getElementsByTagName("tr");
        for (i = 1; i < (lignes.length - 1); i++) {
            devraitEchanger = false;
            x = lignes[i].getElementsByTagName("td")[colonne];
            y = lignes[i + 1].getElementsByTagName("td")[colonne];

            if (isNaN(parseFloat(x.innerHTML))) {
                // Pour trier les chaînes de caractères
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    devraitEchanger = true;
                    break;
                }
            } else {
                // Pour trier les nombres
                if (parseFloat(x.innerHTML) > parseFloat(y.innerHTML)) {
                    devraitEchanger = true;
                    break;
                }
            }
        }

        if (devraitEchanger) {
            lignes[i].parentNode.insertBefore(lignes[i + 1], lignes[i]);
            switching = true;
        }
    }
}
